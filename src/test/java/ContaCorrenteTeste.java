import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import com.itau.funcionarios.dominio.ContaCorrente;


public class ContaCorrenteTeste {
	
	@Test
	public void debitarContaAoRealizarSaque() {
		
		ContaCorrente cc = new ContaCorrente(new BigDecimal("100.00"));
			
		cc.sacar(new BigDecimal("10.00"));
		
		Assert.assertEquals(new BigDecimal("90.00"), cc.obterSaldo());
		
		
	}

}
