package com.bruno.companies.teste;

import org.junit.Assert;
import org.junit.Test;

import com.itau.funcionarios.Calculadora;


public class CalculadorTest {
	
	@Test
	public void dadosDoisNumerosRetorneASoma() {
		Calculadora c = new Calculadora();
			
			Assert.assertEquals(4, c.somar(2,2));
			Assert.assertEquals(5, c.somar(3,2));
	}

}
