package com.itau.funcionarios.dominio;

import java.math.BigDecimal;

public class ContaCorrente {
	private BigDecimal valorInicial;

	public ContaCorrente(BigDecimal valorInicial) {
		this.valorInicial = valorInicial;
	}
	
	public void sacar(BigDecimal saque) {
		valorInicial = valorInicial.subtract(saque);
	}
	
	public BigDecimal obterSaldo() {
		return valorInicial;
		
	}

}
