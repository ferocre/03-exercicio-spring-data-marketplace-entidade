package com.itau.funcionarios.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;

@Entity
@Table(name="jobs")

public class Cargo {
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	
	@Column(name="job_id")
	private int idCargo;
	
	//@Null
	@Column(name="job_title")
	private String nomeCargo;
	
	//@Null
	@Column(name="min_salary")
	private double salarioMinimo;
	
	//@Null
	@Column(name="max_salary")
	private double salarioMaximo;
	
	public int getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(int idCargo) {
		this.idCargo = idCargo;
	}

	public String getNomeCargo() {
		return nomeCargo;
	}

	public void setNomeCargo(String nomeCargo) {
		this.nomeCargo = nomeCargo;
	}

	public double getSalarioMinimo() {
		return salarioMinimo;
	}

	public void setSalarioMinimo(double salarioMinimo) {
		this.salarioMinimo = salarioMinimo;
	}

	public double getSalarioMaximo() {
		return salarioMaximo;
	}

	public void setSalarioMaximo(double salarioMaximo) {
		this.salarioMaximo = salarioMaximo;
	}
	

}
