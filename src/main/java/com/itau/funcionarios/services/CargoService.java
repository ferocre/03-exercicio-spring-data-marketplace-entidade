package com.itau.funcionarios.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.itau.funcionarios.models.Cargo;
import com.itau.funcionarios.models.Departamento;
import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.repositories.CargoRepository;
import com.itau.funcionarios.repositories.DepartamentoRepository;

@Service
public class CargoService {

	@Autowired
	CargoRepository cargoRepository;
	
	
	public Iterable<Cargo> buscarTodos(){
		return cargoRepository.findAll();
	}
	
	public Optional<Cargo> buscarPorId(@PathVariable int id){
		Optional<Cargo> cargo = cargoRepository.findById(id);
		if(cargo.isPresent()) {
			return cargo;
		} else {
			return null;
		}
	}
	
	public Cargo cadastrarCargo(Cargo cargo){
		return cargoRepository.save(cargo);
	}
	
	
}
