package com.itau.funcionarios.services;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.repositories.LocalizacaoRepository;

@Service
public class LocalizacaoService {
	@Autowired
	LocalizacaoRepository localizacaoRepository;
	
	public Iterable<Localizacao> buscarTodos(){
		return localizacaoRepository.findAll();
	}
	
	public Optional<Localizacao> buscarPorId(@PathVariable int id) {
		Optional<Localizacao> localizacao = localizacaoRepository.findById(id);
		if (localizacao.isPresent()) {
			return localizacao;
		} else {
			return null;
		}
	}
	
	public ResponseEntity<Object> atualizarLocalizacao(@RequestBody Localizacao localizacao, @PathVariable int id){
		Optional<Localizacao> local = localizacaoRepository.findById(id);
		if(local.isPresent()){
			localizacaoRepository.save(localizacao);
			return ResponseEntity.noContent().build();
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	
	public ResponseEntity<Object> cadastrarLocalizacao(@RequestBody Localizacao localizacao){
		localizacaoRepository.save(localizacao);
		return (ResponseEntity<Object>) ResponseEntity.created(null);
	}
	
	public void deletarPorId(@PathVariable int id) {
		localizacaoRepository.deleteById(id);
	}
}
