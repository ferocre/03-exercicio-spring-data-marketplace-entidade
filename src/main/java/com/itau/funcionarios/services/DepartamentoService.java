package com.itau.funcionarios.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.itau.funcionarios.models.Departamento;
import com.itau.funcionarios.repositories.DepartamentoRepository;

@Service
public class DepartamentoService {

	@Autowired
	DepartamentoRepository departamentoRepository;
	
	public Iterable<Departamento> buscarTodos(){
		return departamentoRepository.findAll();
	}
	
	public Optional<Departamento> buscarPorId(@PathVariable int id){
		Optional<Departamento> departamento = departamentoRepository.findById(id);
		if(departamento.isPresent()) {
			return departamento;
		} else {
			return null;
		}
	}
}
