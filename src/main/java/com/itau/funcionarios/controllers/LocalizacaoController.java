package com.itau.funcionarios.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.services.LocalizacaoService;

@RestController
@RequestMapping("/localizacao")
public class LocalizacaoController {

	@Autowired
	LocalizacaoService localizacaoservices;
	
	@GetMapping("/lista")
	public Iterable<Localizacao> buscarTodos(){
		return localizacaoservices.buscarTodos();
	}
	
	@GetMapping("/consulta/{id}")
	public Optional<Localizacao> buscarPorId(@PathVariable int id) {
		return localizacaoservices.buscarPorId(id);
	}
	
	@PutMapping("/atualiza/{id}")
	public ResponseEntity<Object> atualizarLocalizacao(@RequestBody Localizacao localizacao, @PathVariable int id){
		   return localizacaoservices.atualizarLocalizacao(localizacao, id);
	}
	
	@PostMapping("/cadastar")
	public ResponseEntity<Object> cadastrarLocalizacao(@RequestBody Localizacao localizacao){
		   return localizacaoservices.cadastrarLocalizacao(localizacao);
	}
	
	@DeleteMapping("/delete/{id}")
	public void deletarPorId(@PathVariable int id) {
		localizacaoservices.deletarPorId(id);
	}
	
}
