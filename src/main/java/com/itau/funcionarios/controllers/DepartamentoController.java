package com.itau.funcionarios.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Departamento;
import com.itau.funcionarios.services.DepartamentoService;

@RestController
@RequestMapping("/departamento")
public class DepartamentoController {
	
	@Autowired
	DepartamentoService departamentoService;
	
	@GetMapping("/lista")
	public Iterable<Departamento> buscarTodos(){
		return departamentoService.buscarTodos();
	}
	
	@GetMapping("consulta/{id}")
	public Optional<Departamento> buscarPorId(@PathVariable int id) {
		return departamentoService.buscarPorId(id);
	}

}
