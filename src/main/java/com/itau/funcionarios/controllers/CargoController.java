package com.itau.funcionarios.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itau.funcionarios.models.Cargo;
import com.itau.funcionarios.models.Departamento;
import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.services.CargoService;
import com.itau.funcionarios.services.DepartamentoService;

@RestController
@RequestMapping("/cargo")

public class CargoController {
	@Autowired
	CargoService cargoService;
	
	@GetMapping("lista")
	public Iterable<Cargo> buscarTodos(){
		return cargoService.buscarTodos();
	}
	
	@GetMapping("consulta/{id}")
	public Optional<Cargo> buscarPorId(@PathVariable int id) {
		return cargoService.buscarPorId(id);
	}
	
	@PostMapping("cadastrar")
	public void cadastrarCargo(@RequestBody Cargo cargo){
		   cargoService.cadastrarCargo(cargo);
	}
}
