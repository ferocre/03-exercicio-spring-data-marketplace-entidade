package com.itau.funcionarios.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Cargo;

public interface CargoRepository extends CrudRepository<Cargo, Integer>{

}
